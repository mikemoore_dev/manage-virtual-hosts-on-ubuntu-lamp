#!/bin/bash

# Remove an unwanted virtual host on Ubuntu LAMP Server

SUFFIX="vbox"

if [ "$(id -u)" != "0" ]; then
	echo "Sorry, you are not root."
	exit 1
fi

# Request the sitename if it has not been passed as a parameter
if [ $# -eq 0 ]
	then 
	echo -e "Please enter a name for your site: "
	read SITENAME;
else
	SITENAME=$1;
fi

APACHE_DIR=/etc/apache2
AV_SITES_DIR="$APACHE_DIR/sites-available"
EN_SITES_DIR="$APACHE_DIR/sites-enabled"

if [[ $SITENAME == *".$SUFFIX"* ]]
then
  	SITE_URL="$SITENAME";
else
	SITE_URL="$SITENAME.$SUFFIX";
fi

read -p "Are you sure you want to completely remove $SITE_URL and all files associated with it? <y/N> " prompt
if [[ $prompt == "y" || $prompt == "Y" || $prompt == "yes" || $prompt == "Yes" ]]
then

	SERVER_ROOT="/var/www"
	SITE_ROOT="/var/www/$SITE_URL"
	SITE_PUBLIC="$SITE_ROOT/public_html"
	SITE_LOGS="$SITE_ROOT/logs"
	SITE_DOCS="$SITE_ROOT/docs"

	# Disable virtual host and remove the configuration file
	EN_CONF_FILE="$EN_SITES_DIR/$SITE_URL.conf"
	CONF_FILE="$AV_SITES_DIR/$SITE_URL.conf"

	if [ -f "$EN_CONF_FILE" ]; then
		echo "Disabling Site";
		echo "";
		a2dissite $SITE_URL.conf;
		rm $CONF_FILE;
	fi

	# Delete site files
	if [ -d "$SITE_ROOT" ]; then
		rm -rf $SITE_ROOT
	fi

	echo "Restarting Apache";
	echo "";
	service apache2 restart;

	echo "Site removal complete, have a great day!";
	echo "";

else
  exit 0
fi