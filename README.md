# Create a virtualhost by running the command 'sudo bash create_vhost.sh'
#
# Completely remove a virtualhost by running the command 'sudo bash remove_vhost.sh'
#
# Follow the prompts in both cases
#
# Parameters:
#
#	Either pass the site name as a single parameter or enter it when prompted
# 