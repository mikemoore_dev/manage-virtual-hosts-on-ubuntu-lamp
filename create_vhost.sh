#!/bin/bash

# Create a new virtual host on Ubuntu LAMP Server

if [ "$(id -u)" != "0" ]; then
	echo "Sorry, you are not root."
	exit 1
fi

# Request the sitename if it has not been passed as a parameter
if [ $# -eq 0 ]
	then 
	echo -e "Please enter a name for your site: "
	read SITENAME;
else
	SITENAME=$1;
fi

APACHE_DIR=/etc/apache2
AV_SITES_DIR="$APACHE_DIR/sites-available"
EN_SITES_DIR="$APACHE_DIR/sites-enabled"

if [[ $SITENAME == *".vbox"* ]]
then
  	SITE_URL="$SITENAME";
else
	SITE_URL="$SITENAME.vbox";
fi

read -p "Your site will be called $SITE_URL, is this correct? <y/N> " prompt
if [[ $prompt == "y" || $prompt == "Y" || $prompt == "yes" || $prompt == "Yes" ]]
then

	SERVER_ROOT="/var/www"
	SITE_ROOT="/var/www/$SITE_URL"
	SITE_PUBLIC="$SITE_ROOT/public_html"
	#SITE_LOGS="$SITE_ROOT/logs"
	SITE_DOCS="$SITE_ROOT/docs"	

	echo -e "Please enter the webmaster's email address: "
	read EMAIL_ADDRESS;

	read -p "You enterred $EMAIL_ADDRESS, is this correct? <y/N> " prompt
	if [[ $prompt == "y" || $prompt == "Y" || $prompt == "yes" || $prompt == "Yes" ]]
	then
		WEBMASTER_EMAIL=$EMAIL_ADDRESS;
	else
		echo "Please try again with a different email address";
		exit;
	fi


	if [ -d "$SITE_ROOT" ]; then
		read -p "$SITE_ROOT already exists, do you wish to continue? <y/N> " prompt
		if [[ $prompt == "y" || $prompt == "Y" || $prompt == "yes" || $prompt == "Yes" ]];
		then
			echo "";
		else
			echo "Please try again with a different site name";
			exit;
		fi
	else
		mkdir $SITE_ROOT
	fi

	if [ -d "$SITE_PUBLIC" ]; then
		read -p "$SITE_PUBLIC already exists, do you wish to continue? <y/N> " prompt
		if [[ $prompt == "y" || $prompt == "Y" || $prompt == "yes" || $prompt == "Yes" ]];
		then
			echo "";
		else
			echo "Please try again with a different site name";
			exit;
		fi
	else
		mkdir $SITE_PUBLIC
	fi

	#if [ -d "$SITE_LOGS" ]; then
	#	read -p "$SITE_LOGS already exists, do you wish to continue? <y/N> " prompt
	#	if [[ $prompt == "y" || $prompt == "Y" || $prompt == "yes" || $prompt == "Yes" ]];
	#	then
	#		echo "";
	#	else
	#		echo "Please try again with a different site name";
	#		exit;
	#	fi
	#else
	#	mkdir $SITE_LOGS
	#fi

	if [ -d "$SITE_DOCS" ]; then
		read -p "$SITE_DOCS already exists, do you wish to continue? <y/N> " prompt
		if [[ $prompt == "y" || $prompt == "Y" || $prompt == "yes" || $prompt == "Yes" ]];
		then
			echo "";
		else
			echo "Please try again with a different site name";
			exit;
		fi
	else
		mkdir $SITE_DOCS
	fi

	# Create a basic holding page
	HOLDING_FILE="$SITE_PUBLIC/index.html"
	HOLDING_HTML="<!doctype html><html lang='en'><head><meta charset='utf-8'><title>$SITENAME</title><meta name='description' content='$SITENAME Holding Page'><meta name='author' content='MikeMooreDev'></head><body><p style='font-family:Arial'>$SITE_URL</p></body></html>";
	if [ -f "$HOLDING_FILE" ]; then
		read -p "$HOLDING_FILE already exists, do you wish to continue? <y/N> " prompt
		if [[ $prompt == "y" || $prompt == "Y" || $prompt == "yes" || $prompt == "Yes" ]];
		then
			echo "Updating holding page";
			echo "";
			rm $HOLDING_FILE
			touch $HOLDING_FILE
			echo $HOLDING_HTML >> $HOLDING_FILE

			echo "Holding page updated";
			echo "";
		else
			echo "Please try again with a different site name";
			exit;
		fi
	else
		echo "Creating holding page";
		echo "";
		touch $HOLDING_FILE
		echo $HOLDING_HTML >> $HOLDING_FILE

		echo "Holding page created";
		echo "";
	fi

	# Create the configuration file
	CONF_FILE="$AV_SITES_DIR/$SITE_URL.conf"
	CONF_FILE_CONTENT="<VirtualHost *:80> \n ServerAdmin $WEBMASTER_EMAIL \n ServerName $SITE_URL \n DocumentRoot $SITE_PUBLIC \n ErrorLog ${APACHE_LOG_DIR}/error.log \n #CustomLog ${APACHE_LOG_DIR}/access.log combined \n <Directory $SITE_PUBLIC> \n Options Indexes FollowSymLinks MultiViews \n AllowOverride All  \n Order allow,deny  \n allow from all  \n </Directory>  \n </VirtualHost>";

	if [ -f "$CONF_FILE" ]; then
		read -p "$CONF_FILE already exists, do you wish to continue? <y/N> " prompt
		if [[ $prompt == "y" || $prompt == "Y" || $prompt == "yes" || $prompt == "Yes" ]];
		then
			echo "Disabling Site";
			echo "";
			a2dissite $SITE_URL.conf;
			echo "Restarting Apache";
			echo "";
			service apache2 restart;
			echo "Updating configuration file";
			echo "";
			rm $CONF_FILE
			touch $CONF_FILE

			echo -e $CONF_FILE_CONTENT >> $CONF_FILE

			echo "Configuration file updated";
			echo "";
		else
			echo "Please try again with a different site name";
			exit;
		fi
	else
		touch $CONF_FILE
		echo -e $CONF_FILE_CONTENT >> $CONF_FILE
	fi

	echo "Enabling Site";
	echo "";
	a2ensite $SITE_URL.conf;
	echo "Restarting Apache";
	echo "";
	service apache2 restart;

	echo "Updating permissions";
	echo "";
	chown -R www-data:www-data "$SITE_ROOT";

	echo "Site creation complete.";
	echo "Add this to your hosts file: 10.0.0.200 $SITE_URL";
	echo "Have a great day!";
	echo "";
	
else
  exit 0
fi